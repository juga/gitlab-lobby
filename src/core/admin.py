from django.contrib import admin

from .models import GitlabAccountRequest
from .gitlab import approve_user


def approve(modeladmin, request, queryset):
    for item in queryset:
        approve_user(item)

approve.short_description = "Approve account requests"

def delete(modeladmin, request, queryset):
    for item in queryset:
        item.delete()

delete.short_description = "Delete account requests"

@admin.register(GitlabAccountRequest)
class GitlabAccountRequestAdmin(admin.ModelAdmin):
    fields = ('username', 'email', 'approved', 'reason')
    list_display = ('username', 'email', 'approved', 'reason')
    actions = [approve, delete]

    def has_delete_permission(self, request, obj=None):
        return False
